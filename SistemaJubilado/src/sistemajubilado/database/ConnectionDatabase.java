/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemajubilado.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tazmadroid
 */
public class ConnectionDatabase {

  private static final String DRIVER_CONNECTION = "com.mysql.jdbc.Driver";
  private static final String URL_DATABASE_DRIVER = "jdbc:mysql://";
  private static final String URL_DATABASE_KEY_USER = "user=";
  private static final String URL_DATABASE_KEY_PASSWORD = "password=";

  private Connection connection = null;
  private Statement statement = null;
  private PreparedStatement preparedStatement = null;
  private ResultSet resultSet = null;
  private String host = "localhost";
  private String databaseName = null;
  private String user = null;
  private String password = null;
  private boolean connected = false;

  public ConnectionDatabase() {
  }

  public ConnectionDatabase(String databaseName) {
    this.databaseName = databaseName;
  }

  public ConnectionDatabase(String databaseName, String user, String password) {
    this.databaseName = databaseName;
    this.user = user;
    this.password = password;
  }

  public ConnectionDatabase(String databaseName, String user, String password,
      String host) {
    this.databaseName = databaseName;
    this.user = user;
    this.password = password;
    this.host = host;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getDatabaseName() {
    return databaseName;
  }

  public void setDatabaseName(String databaseName) {
    this.databaseName = databaseName;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  private String getURLConnection() throws Exception {
    StringBuilder urlConnection = new StringBuilder();
    if (this.databaseName != null && !this.databaseName.isEmpty()
        && this.user != null && !this.user.isEmpty() && this.password != null
        && !this.password.isEmpty()) {
      urlConnection.append(URL_DATABASE_DRIVER);
      urlConnection.append(host);
      urlConnection.append("/");
      urlConnection.append(this.databaseName);
      urlConnection.append("?");
      urlConnection.append(URL_DATABASE_KEY_USER);
      urlConnection.append(user);
      urlConnection.append("&");
      urlConnection.append(URL_DATABASE_KEY_PASSWORD);
      urlConnection.append(password);
    } else {
      throw new Exception("Se requiere establecer el usuario/password de "
          + "la conexión");
    }
    return urlConnection.toString();
  }

  public void connected() {
    try {
      Class.forName(DRIVER_CONNECTION);
      connection = DriverManager.getConnection(getURLConnection());
      if (connection != null) {
        connected = true;
      } else {
        connected = false;
      }
    } catch (Exception e) {
      connected = false;
    }
  }

  public boolean isConnected() {
    return connected;
  }

  public HashMap<String, Object> query(String table,
      HashMap<String, String> fields, String whereClause, List<Object> types)
      throws SQLException {
    HashMap<String, Object> values = new HashMap<>();
    if (isConnected()) {
      StringBuilder query = new StringBuilder("SELECT ");
      String[] keys = new String[fields.keySet().size()];
      fields.keySet().toArray(keys);
      for (int index = 0; index < keys.length; index++) {
        if (index < keys.length - 1) {
          query.append(keys[index]);
          query.append(" AS ");
          query.append(fields.get(keys[index]));
          query.append(", ");
        } else {
          query.append(keys[index]);
          query.append(" AS ");
          query.append(fields.get(keys[index]));
          query.append(" ");
        }
      }
      query.append("FROM ");
      query.append(table);
      if (whereClause != null && !whereClause.isEmpty()) {
        query.append(" ");
        query.append(whereClause);
      }
      query.append(";");
      preparedStatement = connection.prepareStatement(query.toString());
      resultSet = preparedStatement.executeQuery();
      for (int index = 0; index < types.size(); index++) {
        Object type = types.get(index);
        if (type.getClass() == String.class) {
          values.put(fields.get(keys[index]), resultSet.getString(keys[index]));
        } else if (type.getClass() == Integer.class) {
          values.put(fields.get(keys[index]), resultSet.getInt(keys[index]));
        } else if (type.getClass() == Float.class) {
          values.put(fields.get(keys[index]), resultSet.getFloat(keys[index]));
        } else if (type.getClass() == Double.class) {
          values.put(fields.get(keys[index]), resultSet.getDouble(keys[index]));
        } else if (type.getClass() == Boolean.class) {
          values.put(fields.get(keys[index]), resultSet.getBoolean(keys[index]));
        } else if (type.getClass() == Long.class) {
          values.put(fields.get(keys[index]), resultSet.getLong(keys[index]));
        } else if (type.getClass() == Date.class) {
          values.put(fields.get(keys[index]), resultSet.getDate(keys[index]));
        } else {
          values.put(fields.get(keys[index]), null);
        }
      }
    }

    return values;
  }

  public boolean insert(String table, HashMap<String, Object> values) {
    boolean inserted = false;
    try {
      if (isConnected()) {
        StringBuilder insert = new StringBuilder("INSERT INTO ");
        insert.append(table);
        insert.append("(");
        String[] keys = new String[values.keySet().size()];
        values.keySet().toArray(keys);
        for (int index = 0; index < keys.length; index++) {
          if (index < keys.length - 1) {
            insert.append(keys[index]);
            insert.append(",");
          } else {
            insert.append(keys[index]);
          }
        }
        insert.append(")");
        insert.append(" ");
        insert.append("VALUES");
        insert.append("(");
        for (int index = 0; index < values.size(); index++) {
          if (index < keys.length - 1) {
            insert.append("?");
            insert.append(",");
          } else {
            insert.append("?");
          }
        }
        insert.append(")");
        insert.append(";");
        preparedStatement = connection.prepareStatement(insert.toString());
        for (int index = 0; index < values.size(); index++) {
          Object value = values.get(keys[index]);
          if (value.getClass() == String.class) {
            preparedStatement.setString(index + 1, (String) value);
          } else if (value.getClass() == Integer.class) {
            preparedStatement.setInt(index + 1, (int) value);
          } else if (value.getClass() == Float.class) {
            preparedStatement.setFloat(index + 1, (float) value);
          } else if (value.getClass() == Double.class) {
            preparedStatement.setDouble(index + 1, (double) value);
          } else if (value.getClass() == Boolean.class) {
            preparedStatement.setBoolean(index + 1, (boolean) value);
          } else if (value.getClass() == Long.class) {
            preparedStatement.setLong(index + 1, (long) value);
          } else if (value.getClass() == Date.class) {
            preparedStatement.setDate(index + 1, (Date) value);
          }
        }
        int result=preparedStatement.executeUpdate();
        if(result>0){
          inserted=true;
        }else{
          inserted=false;
        }
      } else {
        inserted=false;
      }
    } catch (SQLException e) {
      inserted=false;
    } catch (Exception f) {
      inserted=false;
    }
    return inserted;
  }

  public void close() {
    try {
      if (resultSet != null) {
        resultSet.close();
      }

      if (statement != null) {
        statement.close();
      }

      if (connection != null) {
        connection.close();
      }
    } catch (SQLException ex) {
      Logger.getLogger(ConnectionDatabase.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}
